About
=====

The functest plugin facilitates running JUnit tests inside a running product. The plugin mostly useful
for testing other plugins' APIs and services.

The plugin defines ```junit``` module type for other plugins to expose tests and
a rest endpoint to run those tests along with a ```RemoteTestRunner``` class to easily call
the endpoint and present results during the integration test phase.

QuickStart
==========

Normally you will create a special test plugin, containing junit tests. There is a special ```SpringAwareTestCase```
class that allows wiring spring beans in your test case as if you would wire them in a plugin.

Then you need to define ```junit``` module to expose your tests:

```
<junit key="foo" cliPort="3333" packages="com.atlassian.functest.test">
```

To run the tests, you need to start the product and deploy functest plugin and the test plugin, for example by defining 
```pluginArtifacts``` in AMPS configuration and run a special test runner in integration phase:

```
@RunWith(RemoteTestRunner.class)
public class TestRunner {
    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");

    @RemoteTestRunner.BaseURL
    public static String base() {
        return "http://localhost:" + PORT + CONTEXT + "/";
    }

    @RemoteTestRunner.Group(name = "foo")
    public void fooTest() {
    }
}
```

Contributing
============

Atlassian developers please see the [Cloud Platform Rules of Engagement](http://go.atlassian.com/cproe) for committing to this module.