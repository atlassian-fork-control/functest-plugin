package com.atlassian.functest.error;

import org.junit.Test;

public class ErrorTest {
    @Test
    public void throwingError() {
        throw new Error("This test should be reported as error");
    }
}
