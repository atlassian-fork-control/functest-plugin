package com.atlassian.functest.test;

import com.atlassian.functest.junit.SpringAwareTestCase;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 *
 */
public class SimpleTest extends SpringAwareTestCase {
    @Test
    public void testHelloWorld() {
        assertTrue(true);
    }

    @Test
    @Ignore
    public void testHelloWorldAgain() {
        assertTrue(false);
    }

    @Test
    @Ignore
    public void testHelloWorldAgain2() {
        throw new RuntimeException();
    }
}
