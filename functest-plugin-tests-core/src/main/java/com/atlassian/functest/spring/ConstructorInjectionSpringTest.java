package com.atlassian.functest.spring;

import com.atlassian.functest.junit.SpringAwareTestCase;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConstructorInjectionSpringTest extends SpringAwareTestCase {

    private final PluginAccessor pluginAccessor;

    @Autowired
    public ConstructorInjectionSpringTest(@ComponentImport PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    @Test
    public void injectionWorks() {
        assertThat(pluginAccessor, notNullValue());
    }
}
