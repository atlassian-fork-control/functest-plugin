package com.atlassian.functest.it;

import com.atlassian.functest.client.RemoteTestRunner;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.core.IsNot.not;
import static org.junit.experimental.results.PrintableResult.testResult;
import static org.junit.experimental.results.ResultMatchers.isSuccessful;

public class RemoteTestRunnerTestBase {

    private final File outputDirectory = new File("target/runtest");

    @RunWith(RemoteTestRunner.class)
    public static class RemoteTest {
        private static final String PORT = System.getProperty("http.port", "5990");
        private static final String CONTEXT = System.getProperty("context.path", "/refapp");

        @RemoteTestRunner.BaseURL
        public static String base() {
            return "http://localhost:" + PORT + CONTEXT + "/";
        }
    }

    public static class FailingRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "failing")
        public void failingTest() {
        }
    }

    public static class ErrorRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "error")
        public void failingTest() {
        }
    }

    public static class InjectErrorRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "inject")
        public void failingTest() {
        }
    }

    public static class SuccessfulRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "foo2")
        public void successfulTest() {
        }
    }

    public static class SuccessfulRemoteTwoTests extends RemoteTest {
        @RemoteTestRunner.Group(name = "foo")
        public void successfulTest() {
        }

        @RemoteTestRunner.Group(name = "foo2")
        public void successfulTest2() {
        }
    }

    public static class SpringRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "spring")
        public void springTest() {
        }
    }

    public static class MixedFailingRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "mixed")
        public void springTest() {
        }
    }

    public static class MixedWithExcludeRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "mixed", exclude = {"com.atlassian.functest.mixed.FailingTest"})
        public void springTest() {
        }
    }

    public static class MixedWithIncludeRemoteTest extends RemoteTest {
        @RemoteTestRunner.Group(name = "mixed", include = {"com.atlassian.functest.mixed.PassingTest"})
        public void springTest() {
        }
    }

    @Before
    public void setup() throws Exception {
        cleanOutputDir();
    }

    @After
    public void tearDown() throws Exception {
        cleanOutputDir();
    }

    private void cleanOutputDir() throws IOException {
        FileUtils.deleteDirectory(outputDirectory);
    }

    @Test
    public void failureIsReported() {
        assertThat(testResult(FailingRemoteTest.class), not(isSuccessful()));
    }

    @Test
    public void errorIsReported() {
        assertThat(testResult(ErrorRemoteTest.class), not(isSuccessful()));
    }

    @Test
    public void injectErrorIsReported() {
        assertThat(testResult(InjectErrorRemoteTest.class), not(isSuccessful()));
    }

    @Test
    public void noErrorsIsSuccess() {
        assertThat(testResult(SuccessfulRemoteTest.class), isSuccessful());
    }

    @Test
    public void springTestsAreSuccessful() {
        assertThat(testResult(SpringRemoteTest.class), isSuccessful());
    }

    @Test
    public void onlyOneGroupExecuted() {
        // run a successful test
        testResult(SuccessfulRemoteTest.class);

        String[] directories = outputDirectory.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        // check that only one report directory created
        assertThat(directories, arrayWithSize(1));
    }

    @Test
    public void multipleGroupsExecuted() {
        // run a successful test
        testResult(SuccessfulRemoteTwoTests.class);

        String[] directories = outputDirectory.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        // check that exactly two report directories created
        assertThat(directories, arrayWithSize(2));
    }

    @Test
    public void exclusionAndInclusionIsWorking() throws Exception {
        assertThat(testResult(MixedFailingRemoteTest.class), not(isSuccessful()));
        assertThat("Failing test should be excluded", testResult(MixedWithExcludeRemoteTest.class), isSuccessful());
        assertThat("Only passing test should be included", testResult(MixedWithIncludeRemoteTest.class), isSuccessful());
    }
}