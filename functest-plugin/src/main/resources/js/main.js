jQuery(document).ready(function() {
    function runTest(classInfo) {
        var status = jQuery('#' + classInfo.hash+'-status');
        status.css('background', 'yellow');
        status.html("Running");

        jQuery.ajax({
            url: contextPath + '/rest/functest/1.0/junit/runTests.json?outdir=target&includes=' + classInfo.name,
            success: function(data){
                if (data.result == 0) {
                    status.html('Success (' + data.results[classInfo.name].passed + ' tests)');
                    status.css('background', 'green');
                } else {
                    var test = data.results[classInfo.name];
                    status.html('Failed (' + test.passed +
                            ' passed, ' + (test.failed + test.errors) + ' failed)');

                    AJS.$("#" + classInfo.hash + "-output").click(function() {
                        var popup = new AJS.Dialog({width: 1200, height: 700, closeOnOutsideClick: true});
                        popup.addHeader(classInfo.name + " Result Details");
                        var sum = "<ul>";
                        for (var x in test.testMethods) {
                            sum += "<li>" + x + " - ";
                            switch (test.testMethods[x]) {
                                case 0 : sum += "SUCCESS"; break;
                                case 1 : sum += "FAILURE"; break;
                                case 2 : sum += "ERROR"; break;
                            }
                            sum += "</li>";
                        }
                        sum += "</ul>";
                        popup.addPanel("Summary", sum, "panel-body");
                        popup.addPanel("Std Out", "<pre>" + data.output + "</pre>", "panel-body");
                        popup.show();
                    });

                    status.css('background', 'red');
                    //status.html(data.output);
                }
              }});
        return false;
    }
    function reloadGroup(groupName, cliPort, nextFunction) {
        var status = jQuery('#' + groupName+'-status');
        status.css('background', 'yellow');
        status.html("Reloading");

        jQuery.ajax({
            url : contextPath + '/rest/functest/1.0/cli',
            type : 'POST',
            data: {
                command : 'pi',
                cliPort : cliPort,
                group   : groupName
            },
            success : function(data){
                status.html("Success");
                status.css('background', 'green');
                if (nextFunction) {
                    nextFunction();
                }
              },
            error : function(xhr, msg, ex) {
                status.html("Failure: " + xhr.responseText);
                status.css('background', 'red');
            }});
        return false;
    }

    function runAll(tests) {
        jQuery.each(tests, function(i, classInfo) {
            runTest(classInfo);});
    }
    
    jQuery.each(groups, function(groupName, groupData) {
        jQuery('#' + groupName + '-reload').click(function() {reloadGroup(groupName, groupData.cliPort);});
        jQuery('#' + groupName + "-run-all").click(function() {runAll(groupData.tests); });
        jQuery('#' + groupName + "-reload-and-run-all").click(function() {
            reloadGroup(groupName, groupData.cliPort, function() {
                runAll(groupData.tests);
            });
        });
        jQuery.each(groupData.tests, function(i, classInfo) {
            jQuery('#' +classInfo.hash + '-run').click(function() {runTest(classInfo);});
            jQuery('#' +classInfo.hash + '-reload-and-run').click(
                    function() {
                        reloadGroup(groupName, groupData.cliPort,
                                function() {
                                    runTest(classInfo);
                                });
                    });
        });
    });


});