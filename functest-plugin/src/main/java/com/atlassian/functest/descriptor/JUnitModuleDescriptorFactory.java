package com.atlassian.functest.descriptor;


import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;

import javax.inject.Inject;
import javax.inject.Named;

@ModuleType(ListableModuleDescriptorFactory.class)
@Named("junitModuleDescriptorFactory")
public class JUnitModuleDescriptorFactory extends SingleModuleDescriptorFactory<JUnitModuleDescriptor> {
    @Inject
    public JUnitModuleDescriptorFactory(final HostContainer hostContainer) {
        super(hostContainer, "junit", JUnitModuleDescriptor.class);
    }
}