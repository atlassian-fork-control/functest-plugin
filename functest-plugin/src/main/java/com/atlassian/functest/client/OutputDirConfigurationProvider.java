package com.atlassian.functest.client;

import org.junit.runners.model.TestClass;

import java.lang.annotation.Annotation;

class OutputDirConfigurationProvider extends BaseConfigurationProvider<RemoteTestRunner.OutputDir> {

    static final String DEFAULT_OUTPUT_DIR = "target/runtest";

    OutputDirConfigurationProvider(TestClass testClass) {
        super(testClass, RemoteTestRunner.OutputDir.class);
    }

    @Override
    protected String getAnnotationValue(Annotation a) {
        return ((RemoteTestRunner.OutputDir)a).value();
    }

    @Override
    protected String getFallbackValue() {
        return DEFAULT_OUTPUT_DIR;
    }
}
