package com.atlassian.functest.client;

import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.TestClass;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

abstract class BaseConfigurationProvider<T extends Annotation> implements AnnotationValidator, ConfigurationProvider<String> {

    private final List<Throwable> errors;
    private final String value;

    BaseConfigurationProvider(TestClass testClass, Class<T> t) {

        this.errors = new ArrayList<>();

        String value = null;

        for (Annotation a : testClass.getAnnotations()) {
            if (a.getClass().equals(t.getClass())) {
                if (value != null) {
                    errors.add(new RuntimeException("Only single " + t.toString() + " annotation allowed"));
                    break;
                } else {
                    value = getAnnotationValue(a);
                }
            }
        }

        // Look for static method
        List<FrameworkMethod> m = testClass.getAnnotatedMethods(t);
        if (value != null && !m.isEmpty()) {
            errors.add(new RuntimeException("Only single " + t.toString() + " annotation allowed"));
        } else if (!m.isEmpty()) {
            FrameworkMethod call = m.get(0);
            Method fMethod = call.getMethod();

            errors.addAll(MethodValidator.validate(String.class, fMethod));

            if (errors.isEmpty()) {
                try {
                    value = (String) call.invokeExplosively(null);
                } catch (Throwable e) {
                    errors.add(e);
                }
            }
        }

        this.value = (value == null ? getFallbackValue() : value);
    }

    @Override
    public List<Throwable> getErrors() {
        return this.errors;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    protected abstract String getAnnotationValue(Annotation a);

    protected abstract String getFallbackValue();
}
