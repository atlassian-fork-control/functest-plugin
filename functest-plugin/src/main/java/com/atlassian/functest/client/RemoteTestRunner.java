package com.atlassian.functest.client;

import com.atlassian.functest.rest.TestDescription;
import com.atlassian.functest.rest.TestGroup;
import com.atlassian.functest.rest.TestResult;
import com.atlassian.functest.rest.TestResult.TestStatus;
import com.atlassian.functest.rest.TestResults;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.junit.internal.AssumptionViolatedException;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * JUnit test runner that calls functest over REST
 *
 * @author pandronov
 */
public class RemoteTestRunner extends ParentRunner<GroupRunner> {

    /**
     * The <code>BaseURL</code> annotation specifies the basic URL of target container where functest-plugin REST api is
     * located. Could be provided for whole type or by method, but suit should has only single base url value
     */
    @Inherited
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface BaseURL {
        /**
         * @return the classes to be run
         */
        public String value() default "";
    }

    /**
     * The <code>OutputDir</code> annotation specifies the output directory for test results.
     */
    @Inherited
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface OutputDir {
        /**
         * @return the classes to be run
         */
        public String value() default "";
    }

    /**
     * Definition of the <code>test group</code> to be launched. That is annotation to be used instead of {@link org.junit.Test}
     *
     * @author pandronov
     * @since 2.4
     */
    @Inherited
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Group {
        /**
         * @return group to be launched
         */
        String name();

        /**
         * @return list of tests to be launched
         */
        String[] include() default {};

        /**
         * @return list of tests in the group to be excluded
         */
        String[] exclude() default {};
    }

    /**
     * URL prefix for all REST calls
     */
    final
    @Nonnull
    String base;

    /**
     * OutputPath for test results
     */
    final
    @Nonnull
    String outputDir;

    /**
     * Configure remote test suit based on provided class
     *
     * @param testClass with suite settings annotations
     */
    public RemoteTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);

        BaseURLConfigurationProvider baseURLConfigurationProvider = new BaseURLConfigurationProvider(getTestClass());
        OutputDirConfigurationProvider outputDirConfigurationProvider = new OutputDirConfigurationProvider(getTestClass());

        List<Throwable> errors = new ArrayList<>();
        errors.addAll(baseURLConfigurationProvider.getErrors());
        errors.addAll(outputDirConfigurationProvider.getErrors());

        if (!errors.isEmpty()) {
            throw new InitializationError(errors);
        }

        this.base = baseURLConfigurationProvider.getValue();
        this.outputDir = outputDirConfigurationProvider.getValue();
    }


    /*
     * (non-Javadoc)
     * @see org.junit.runners.ParentRunner#getChildren()
     */
    @Override
    @NotNull
    protected List<GroupRunner> getChildren() {
        Client c = Client.create();

        // Retrieve only the groups we are interested in
        MultivaluedMap<String, String> restQueryParameters = new MultivaluedMapImpl();
        for (FrameworkMethod m : getTestClass().getAnnotatedMethods(Group.class)) {
            String testGroupName = m.getAnnotation(Group.class).name();
            restQueryParameters.add("groups", testGroupName);
        }

        WebResource remoteTests = c.resource(UriBuilder.fromUri(base).build())
                .path("rest").path("functest").path("1.0").path("junit").path("getTests")
                .queryParams(restQueryParameters);

        Map<String, List<TestDescription>> remoteTestsDescriptions = new HashMap<>();
        TestGroup[] testGroups = remoteTests.get(TestGroup[].class);
        for (TestGroup testGroup : testGroups) {
            remoteTestsDescriptions.put(testGroup.getName(), testGroup.getTests());
        }

        // Create group running based on the running class config
        List<GroupRunner> result = new LinkedList<>();
        for (FrameworkMethod frameworkMethod : getTestClass().getAnnotatedMethods(Group.class)) {
            Group grp = frameworkMethod.getAnnotation(Group.class);
            if (!remoteTestsDescriptions.containsKey(grp.name())) {
                throw new RuntimeException("Group configured to be run but is not installed on the remote site: " + grp.name());
            }

            List<TestDescription> tests = remoteTestsDescriptions.get(grp.name());
            result.add(new GroupRunner(frameworkMethod.getName(), grp, tests));
        }
        return result;
    }

    /**
     * Return Junit description for the given group of tests
     *
     * @param grp group of tests to build description for
     * @return junit description of a test group
     */
    @Override
    @NotNull
    protected Description describeChild(@NotNull GroupRunner grp) {
        Description dsk = Description.createSuiteDescription(String.format("%s: %s", grp.getName(), grp.getGroup().name()));
        for (TestDescription t : grp) {
            dsk.addChild(describeChild(grp, t));
        }
        return dsk;
    }

    /**
     * Build description for a given test belongs to the known tests group
     *
     * @param r    test group
     * @param test test
     * @return junit description
     */
    @NotNull
    protected Description describeChild(@NotNull GroupRunner r, @NotNull TestDescription test) {
        Description td = Description.createSuiteDescription(test.getTestClass());
        for (String method : test) {
            td.addChild(describeChild(r, test, method));
        }
        return td;
    }

    /**
     * Builds junit description for the single tests belongs to the known group and class
     *
     * @param r      test group
     * @param test   test class
     * @param method test method
     * @return junit description
     */
    protected Description describeChild(@NotNull GroupRunner r, @NotNull TestDescription test, @NotNull String method) {
        return Description.createSuiteDescription(String.format("%s(%s)", method, test.getTestClass()));
    }

    /**
     * Launch group of tests on remote service and updates notifier with results
     *
     * @param grp      group to be launched
     * @param notifier Junit controller
     */
    @Override
    protected void runChild(@NotNull GroupRunner grp, @NotNull RunNotifier notifier) {
        Description gd = describeChild(grp);
        notifier.fireTestStarted(gd);

        final MultivaluedMap<String, String> restQueryParameters = new MultivaluedMapImpl();
        final Group group = grp.getGroup();
        restQueryParameters.add("groups", group.name());
        restQueryParameters.add("outdir", new File(outputDir).getAbsolutePath());
        restQueryParameters.put("includes", Arrays.asList(group.include()));
        restQueryParameters.put("excludes", Arrays.asList(group.exclude()));

        try {
            // Launch remote tests
            Client c = Client.create();
            WebResource target = c.resource(UriBuilder.fromUri(base).build())
                    .path("rest").path("functest").path("1.0").path("junit").path("runTests")
                    .queryParams(restQueryParameters);
            TestResults results = target.get(TestResults.class);

            // update notifier
            Map<String, TestResult> tr = results.results;
            for (TestDescription t : grp) {
                // group contains description of ALL tests classes while results contains only those which are product of "excludes" and "includes" options
                if (tr.containsKey(t.getTestClass())) {
                    runChild(grp, t, tr, notifier);
                }
            }

        } catch (AssumptionViolatedException e) {
            notifier.fireTestAssumptionFailed(new Failure(gd, e));
        } catch (Exception e) {
            notifier.fireTestFailure(new Failure(gd, e));
        } finally {
            notifier.fireTestFinished(gd);
        }
    }

    /**
     * Assert that there is no initialisation errors.
     * <p>
     * When InitialisationError happened remotely, test methods in TestResult will not have errors,
     * but the overall status will be !0
     *
     * @param t      test description
     * @param result test class run result
     * @throws InitializationError
     */
    private void assertNoIntialisationError(TestDescription t, TestResult result) throws InitializationError {
        if (result.getStatus() == 0) {
            // No failures
            return;
        }

        for (TestStatus testStatus : result.testMethods.values()) {
            if (!testStatus.fErrors.isEmpty() ||
                    !testStatus.fFailures.isEmpty()) {
                // At least one test completed with error
                return;
            }
        }

        throw new InitializationError("Failed to initialise " + t.getTestClass());
    }


    /**
     * Update JUnit controller with the test results for the given tests class
     *
     * @param grp      group test class belongs to
     * @param t        tests class
     * @param results  test run results
     * @param notifier junit controller
     */
    private void runChild(GroupRunner grp, TestDescription t, Map<String, TestResult> results, RunNotifier notifier) {
        Description td = describeChild(grp, t);
        notifier.fireTestStarted(td);
        try {
            String clazz = t.getTestClass();
            TestResult result = results.get(clazz);
            assertNoIntialisationError(t, result);
            for (String m : t) {
                runChild(grp, t, m, result, notifier);
            }
        } catch (AssumptionViolatedException e) {
            notifier.fireTestAssumptionFailed(new Failure(td, e));
        } catch (Exception e) {
            notifier.fireTestFailure(new Failure(td, e));
        } finally {
            notifier.fireTestFinished(td);
        }
    }

    /**
     * Update JUnit controller with the result of a single test method
     *
     * @param grp      group test class belongs to
     * @param t        tests class
     * @param m        test method
     * @param result   test run results
     * @param notifier junit controller
     */
    private void runChild(GroupRunner grp, TestDescription t, String m, TestResult result, RunNotifier notifier) {
        Description td = describeChild(grp, t, m);
        notifier.fireTestStarted(td);
        try {
            TestStatus status = (result != null ? result.testMethods.get(m) : null);
            if (status == null) {
                notifier.fireTestIgnored(td);
            } else if (!status.fErrors.isEmpty()) {
                notifier.fireTestFailure(new Failure(td, status.fErrors.get(0)));
            } else if (!status.fFailures.isEmpty()) {
                notifier.fireTestFailure(new Failure(td, status.fFailures.get(0)));
            }
        } catch (AssumptionViolatedException e) {
            notifier.fireTestAssumptionFailed(new Failure(td, e));
        } catch (Exception e) {
            notifier.fireTestFailure(new Failure(td, e));
        } finally {
            notifier.fireTestFinished(td);
        }
    }
}
