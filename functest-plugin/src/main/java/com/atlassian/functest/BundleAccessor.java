package com.atlassian.functest;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;


/**
 * Used to get the classes matching a specific pattern from the BundleContext.
 */
@Named("bundleAccessor")
public class BundleAccessor {
    private static Logger log = LoggerFactory.getLogger(BundleAccessor.class);
    private final BundleContext bundleContext;

    @Inject
    public BundleAccessor(final BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    /**
     * Finds the application context for a plugin.  Can return null;
     */
    public ApplicationContext getApplicationContextForPlugin(String pluginKey) {
        try {
            ServiceReference[] refs = bundleContext.getServiceReferences(ApplicationContext.class.getName(), null);

            for (ServiceReference ref : refs) {
                String serviceBundleKey = (String) ref.getBundle().getHeaders().get(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
                if (pluginKey.equals(serviceBundleKey)) {
                    return (ApplicationContext) bundleContext.getService(ref);
                }
            }

        } catch (InvalidSyntaxException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public Collection<Class<?>> getClassesMatching(BundleContext bundleContext, String testPackage) {
        final Collection<Class<?>> classesInPackage = new ArrayList<Class<?>>();

        String pattern = "*.class";
        String start = testPackage.replace('.', '/');

        Enumeration<URL> matchedEntries = bundleContext.getBundle().findEntries(start, pattern, true);
        if (matchedEntries != null) {
            for (URL url : Collections.list(matchedEntries)) {
                String className = url.getFile();
                className = className.substring(1, className.indexOf(".class"));
                className = className.replace("/", ".");
                try {
                    classesInPackage.add(bundleContext.getBundle().loadClass(className));
                } catch (ClassNotFoundException e) {
                    log.error("Unable to load class: '" + className + "'", e);
                }

            }
        }
        return classesInPackage;
    }
}
