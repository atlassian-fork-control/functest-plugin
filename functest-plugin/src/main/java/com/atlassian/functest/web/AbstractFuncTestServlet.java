package com.atlassian.functest.web;

import com.atlassian.functest.web.util.HtmlSafeContent;
import com.atlassian.functest.web.util.RendererContextBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Abstract base class for servlets, offering exception handling,
 * template rendering (with automatic web resource inclusion), logging
 * and some convenience methods.
 *
 * @since v3.0
 */
abstract class AbstractFuncTestServlet extends HttpServlet {
    @ComponentImport
    protected final WebResourceManager webResourceManager;
    @ComponentImport
    protected final TemplateRenderer templateRenderer;

    @Inject
    public AbstractFuncTestServlet(final TemplateRenderer templateRenderer,
                                   final WebResourceManager webResourceManager) {
        this.templateRenderer = templateRenderer;
        this.webResourceManager = webResourceManager;
    }

    /**
     * Implement this method to get one of more Plugin Web Resources included
     * in the rendered page. Must not return {@code null}.
     *
     * @return a list of web resource keys
     * (e.g. {@code "com.atlassian.applinks.applinks-plugin:basic-js"})
     */
    protected List<String> getRequiredWebResources() {
        return Collections.emptyList();
    }

    /**
     * @return an empty and immutable velocity render context.
     */
    protected final Map<String, Object> emptyContext() {
        return Collections.emptyMap();
    }

    protected void render(final String template, final Map<String, Object> renderContext,
                          final HttpServletResponse response)
            throws IOException {
        for (String resource : getRequiredWebResources()) {
            webResourceManager.requireResource(resource);
        }
        RendererContextBuilder builder = new RendererContextBuilder(renderContext)
                .put("webResources", (HtmlSafeContent) webResourceManager::getRequiredResources);
        response.setContentType("text/html; charset=utf-8");
        templateRenderer.render(template, builder.build(), response.getWriter());
    }


}
