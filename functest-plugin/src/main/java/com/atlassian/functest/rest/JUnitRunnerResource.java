package com.atlassian.functest.rest;


import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.functest.BundleAccessor;
import com.atlassian.functest.ClassScanner;
import com.atlassian.functest.descriptor.JUnitModuleDescriptor;
import com.atlassian.functest.junit.SpringAwareJUnit4ClassRunner;
import com.atlassian.functest.util.ChainingClassLoader;
import com.atlassian.functest.util.PluginMetaData;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitResultFormatter;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitTest;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitTestRunner;
import org.apache.tools.ant.taskdefs.optional.junit.PlainJUnitResultFormatter;
import org.apache.tools.ant.taskdefs.optional.junit.XMLJUnitResultFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.slf4j.Logger;
import org.springframework.context.ApplicationContext;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@AnonymousAllowed
@Path("/junit")
public class JUnitRunnerResource {

    private static final Logger LOG = PluginMetaData.instance().getLogger();

    private final BundleAccessor bundleAccessor;
    private final ClassScanner classScanner;

    public JUnitRunnerResource(final BundleAccessor bundleAccessor, final ClassScanner classScanner) {
        this.bundleAccessor = bundleAccessor;
        this.classScanner = classScanner;
    }

    /**
     * Sets system properties for the test
     */
    @POST
    @XsrfProtectionExcluded
    @Path("systemProperty")
    public Response systemProperty(@Context UriInfo info) {
        for (String key : info.getQueryParameters().keySet()) {
            System.setProperty(key, info.getQueryParameters().getFirst(key));
        }
        return Response.ok().build();
    }

    /**
     * Calculate exact list of tests to be launch with the given filters in each group
     *
     * @return mapping from group name to the list of tests in the group
     */
    @GET
    @Path("getTests")
    public TestGroup[] getTests(@QueryParam("includes") List<String> includes,
                                @QueryParam("excludes") List<String> excludes,
                                @QueryParam("groups") List<String> groups) {

        // Scan classes
        Map<JUnitModuleDescriptor, List<Class<?>>> map = classScanner.findTestClassesByDescriptor(groups, includes, excludes);

        // build resultset
        int count = 0;
        TestGroup[] result = new TestGroup[map.size()];
        for (Map.Entry<JUnitModuleDescriptor, List<Class<?>>> entry : map.entrySet()) {
            TestGroup grp = new TestGroup();
            grp.setName(entry.getKey().getKey());
            for (Class<?> clazz : entry.getValue()) {
                try {
                    // find a runner to scan the test class
                    final Class<? extends Runner> runnerClass;
                    if (clazz.isAnnotationPresent(RunWith.class)) {
                        final Class<? extends Runner> customRunnerClass = clazz.getAnnotation(RunWith.class).value();
                        if (BlockJUnit4ClassRunner.class.isAssignableFrom(customRunnerClass)) {
                            runnerClass = customRunnerClass;
                        } else {
                            LOG.warn("Runner class {} does not extend BlockJUnit4ClassRunner. Will use BlockJUnit4ClassRunner instead and hope for the best.");
                            runnerClass = BlockJUnit4ClassRunner.class;
                        }
                    } else {
                        runnerClass = BlockJUnit4ClassRunner.class;
                    }

                    final BlockJUnit4ClassRunner runner = (BlockJUnit4ClassRunner) runnerClass.getConstructor(Class.class).newInstance(clazz);

                    // Read all tests in the class
                    List<String> tests = new ArrayList<>();

                    for (FrameworkMethod m : runner.getTestClass().getAnnotatedMethods(Test.class)) {
                        tests.add(m.getName());
                    }

                    // Add test into the group
                    grp.add(new TestDescription(clazz.getCanonicalName(), tests));
                } catch (Exception e) {
                    LOG.error("Failed to scan test class: {}", clazz, e);
                }
            }
            result[count++] = grp;
        }

        return result;
    }

    @GET
    @Path("runTests")
    public Response runTests(@QueryParam("includes") List<String> includes,
                             @QueryParam("excludes") List<String> excludes,
                             @QueryParam("groups") List<String> groups,
                             @QueryParam("outdir") String outdir) {

        if (outdir == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Missing outdir parameter").build();
        }

        final Map<JUnitModuleDescriptor, List<Class<?>>> classesByGroup = classScanner.findTestClassesByDescriptor(groups, includes, excludes);
        if (classesByGroup.isEmpty()) {
            LOG.info("No test groups found.");
            return Response.ok(new TestResults(JUnitTestRunner.SUCCESS, "No test groups.", outdir)).build();
        }

        final File outdirFile = new File(outdir);
        LOG.info("Test results will be written to: " + outdirFile.getAbsolutePath());

        final ByteArrayOutputStream streamOut = new ByteArrayOutputStream();

        final TestResults results = new TestResults(outdirFile.getAbsolutePath());
        results.setResult(JUnitTestRunner.SUCCESS);

        for (Map.Entry<JUnitModuleDescriptor, List<Class<?>>> entry : classesByGroup.entrySet()) {
            String groupName = entry.getKey().getKey();
            Collection<Class<?>> filtered = entry.getValue();
            if (filtered.isEmpty()) {
                LOG.info("No matching tests found in group '" + groupName + "'. Please ensure you have the includes and excludes set correctly.");
            } else {
                LOG.info("The following tests will be run for group '" + groupName + "':");
                for (Class<?> aClass : filtered) {
                    LOG.info("*\t" + aClass.getName());
                }
                File groupOutputDir = new File(outdirFile, groupName + "-reports");
                groupOutputDir.mkdirs();

                String pluginKey = PluginMetaData.instance().getPluginKeyForClass(filtered.iterator().next());
                ApplicationContext applicationContext = bundleAccessor.getApplicationContextForPlugin(pluginKey);
                if (applicationContext != null) {
                    // Yes, this means you can only process one request at a time
                    SpringAwareJUnit4ClassRunner.setApplicationContext(applicationContext);
                } else {
                    throw new IllegalStateException("Unable to find spring context for plugin '" + pluginKey + "'");
                }


                for (Class<?> aClass : filtered) {
                    TestResult result = runSingleTest(streamOut, aClass, groupOutputDir);
                    results.addResult(aClass.getName(), result);
                }
                LOG.info("Test results for group '" + groupName + "' found in " + groupOutputDir.getAbsolutePath());
            }
        }
        LOG.info(streamOut.toString());
        results.setOutput(streamOut.toString());

        return Response.ok(results).build();
    }

    private TestResult runSingleTest(ByteArrayOutputStream streamOut, Class testClass, File outdir) {
        JUnitTest test = new JUnitTest(testClass.getName());
        TestResult result = new TestResult();
        FileOutputStream xmlOut = null;
        FileOutputStream txtOut = null;
        try {
            final File xmlFile = new File(outdir, "TEST-" + test.getName() + ".xml");
            final File txtFile = new File(outdir, test.getName() + ".txt");

            final JUnitResultFormatter responseFormatter = new PlainJUnitResultFormatter();
            responseFormatter.setOutput(streamOut);

            final XMLJUnitResultFormatter xmlFormatter = new XMLJUnitResultFormatter();
            xmlOut = new FileOutputStream(xmlFile);
            xmlFormatter.setOutput(xmlOut);

            final PlainJUnitResultFormatter textFileFormatter = new PlainJUnitResultFormatter();
            txtOut = new FileOutputStream(txtFile);
            textFileFormatter.setOutput(txtOut);

            final PlainJUnitResultFormatter logTextFormatter = new PlainJUnitResultFormatter();
            ByteArrayOutputStream logOutput = new ByteArrayOutputStream();
            textFileFormatter.setOutput(logOutput);

            final RestJUnitResultFormatter restFormatter = new RestJUnitResultFormatter(result);


            JUnitTestRunner runner = makeRunnerFor(test, testClass);
            runner.addFormatter(xmlFormatter);
            runner.addFormatter(responseFormatter);
            runner.addFormatter(textFileFormatter);
            runner.addFormatter(restFormatter);
            runner.addFormatter(logTextFormatter);

            LOG.info("Running test " + testClass.getName());

            runner.run();

            LOG.info("Test {} complete with result of {}", testClass.getName(), runner.getRetCode());

            if (runner.getRetCode() != 0) {
                LOG.error(new String(logOutput.toByteArray()));
            }

            result.setStatus(runner.getRetCode());

        } catch (FileNotFoundException e) {
            LOG.error(e.getMessage(), e);
            result.setStatus(JUnitTestRunner.ERRORS);
        } finally {
            closeQuietly(xmlOut);
            closeQuietly(txtOut);
        }

        return result;
    }

    private static void closeQuietly(OutputStream stream) {
        try {
            if (stream != null) {
                stream.close();
            }
        } catch (IOException ignored) {
            // ignored
        }
    }

    private JUnitTestRunner makeRunnerFor(JUnitTest test, Class testClass) {
        JUnitTestRunner runner = new JUnitTestRunner(test, false, true, false, true,
                new ChainingClassLoader(testClass.getClassLoader(), getClass().getClassLoader()));
        try {
            Field forkField = runner.getClass().getDeclaredField("forked"); // set this so we can capture stdout,stderr
            forkField.setAccessible(true);
            forkField.setBoolean(runner, true);
        } catch (NoSuchFieldException e) {
            LOG.warn(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            LOG.warn(e.getMessage(), e);
        }
        return runner;
    }
}
