package com.atlassian.functest.rest;

public class FunctestNonSerializableException extends Exception {

    public FunctestNonSerializableException(Throwable cause) {
        super(String.format("Non-serializable exception %s: %s", cause.getClass().getName(), cause.getMessage()));
        setStackTrace(cause.getStackTrace());
    }
}
