package com.atlassian.functest.client;

public class FailingTestCase {

    @RemoteTestRunner.BaseURL
    private String base() {
        return "";
    }

    @RemoteTestRunner.OutputDir
    protected static Long outputDir() {
        return 1L;
    }

}
