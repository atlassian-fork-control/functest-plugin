package com.atlassian.functest.client;

public class DummyTestCase {

    static final String BASE_URL = "http://localhost";
    static final String OUTPUT_DIR = "tmp/test-results";

    @RemoteTestRunner.BaseURL
    public static String base() {
        return BASE_URL;
    }

    @RemoteTestRunner.OutputDir
    public static String outputDir() {
        return OUTPUT_DIR;
    }

}
